﻿using HarmonyLib;
using RimWorld;
using System;
using Verse;

namespace RevealingApparel
{
    [StaticConstructorOnStartup]
    public static class HarmonyPatching
    {

        static HarmonyPatching()
        {
            Harmony harmony = new Harmony("RevealingApparel");
            harmony.PatchAll();

        }
    }
}
