# Revealing Apparel

This allows displaying bodyAddons under clothing, useful for cases where something covers for example the "Torso" zone in code, but not graphically, e.g. the Flak Vest.
It is not necessary to list every bodypart, only those you want to reveal because they are covered (E.g., crotchless pants that cover "Legs" would only need an entry for "Genitals/FeaturelessCrotch") More won't break anything, but you can save yourself some work.
The code works this way:

- The CanDraw() method of AlienRaces returns a false, my code then checks if CanDrawRevealing() might be true
- It makes a list of all clothes the character is wearing that would cover the bodyAddon, using the bodypartgroup (Eg. Torso, Legs) as defined in the bodyAddon's hiddenUnderApparelFor field
- If every clothing item on that list has a revealingPath entry matching both the path defined for the bodyAddon and our pawn's bodytpe, the bodyAddon gets drawn

This should work for any type of bodyaddon, e.g. tails too.
